#include <iostream>
using namespace std;
#include <cmath>

int main()
{
    //checking if year is a leap year or not
    int year, startDay, type, days, month(1);
    cout << "Enter the year you want to display? ";
    cin >> year;
    
    if((year % 400 == 0) || (year % 100 == 0) ^ (year % 4 == 0))
    {
        type = 1;
        cout << "\nThis year is a leap year!";
    }
    else
    {
        type = 0;
        cout << "\nThis is not a leap year.";
    }
    //enter the number of days of the month
    cout << "\nEnter the first day of January: sunday for 1, tuesday for 2, etc  ";
    cin >> startDay;
    while(startDay < 0 || startDay >12)
    {
        cout << "Invalid";
        cin >> startDay;
    }
    while(month <= 12)
    {
        switch (month)
        {
        case 1:
            cout << "\n\nJanuary\n";
            days = 31;
            break;
        case 2:
            cout << "\n\nFebruary\n";
            if(type = 1)
            {
                days = 29;
            }
            else
            {
                days = 28;
            }
            break;
        case 3:
            cout << "\n\nMarch\n";
            days = 31;
            break;
        case 4:
            cout << "\n\nApril\n";
            days = 30;
            break;
        case 5:
            cout << "\n\nMay\n";
            days = 31;
            break;
        case 6: 
            cout << "\n\nJune\n";
            days = 30;
            break;
        case 7:
            cout << "\n\nJuly\n";
            days = 31;
            break;
        case 8:
            cout << "\n\nAugust\n";
            days = 31;
            break;
        case 9:
            cout << "\n\nSeptember\n";
            days = 30;
            break;
        case 10:
            cout << "\n\nOctober\n";
            days = 31;
            break;
        case 11:
            cout << "\n\nNovember\n";
            days = 30;
            break;
        case 12:
            cout << "\n\nDecember\n";
            days = 31;
            break;
        
        default:
            break;
        }
        cout << endl << "Sun\tMon\tTue\tWed\tThur\tFri\tSat\n";
        for(int i = 1; i<startDay; i++)
        {
            cout << "\t";
        }
        for(int j = 1; j<=days; j++)
        {
            if(((j + startDay - 2) % 7 == 0) && j != 1)
            cout << endl;
            cout << j << "\t";
        }
        cout << endl << endl;
        month++;
    }
    return 0;
    
}